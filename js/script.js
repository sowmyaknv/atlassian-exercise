angular.module('atlassianApp', [])
  .controller('mainController', ['$scope', 'sessionJSON', function($scope, sessionJSON) {

    _.defaults($scope, {
        Tabs: []
    });

    /*
        Function to load the sessions associated with the selected active tab (track)
     */
    $scope.setTrack = function(NewTab) {
        $scope.ActiveTab = NewTab;
        $scope.sessions = [];
        _.each($scope.Items, function(Item) {
            if(Item.Track.Title === NewTab) {
                if(Item.Speakers) {
                    Item.speaker = _.first(Item.Speakers);
                    Item.speakersCount = Item.Speakers.length;
                }
                $scope.sessions.push(Item);
            }
        });
        //Load the first session details when a tab (track) is selected
        $scope.loadSessionDetails(_.first($scope.sessions).Id);
    };

    /*
        Function to track the active selected tab (track) and set it to ActiveTab
     */
    $scope.isTrackSet = function(TabNum) {
        return $scope.ActiveTab === TabNum;
    };

    /*
        Function to load the session details along with the speaker(s) details for the tab (track) selected
     */
    $scope.loadSessionDetails = function(Id) {
        $scope.SelectedSession = _.find($scope.Items, function(Item) {
            return Item.Id === Id;
        });
    };

    //Kick start the page
    $scope.load = function() {
        var Tabs = [];
        $scope.Items = sessionJSON.Items;
        _.each(sessionJSON.Items, function(Item) {
            var Tab = {};
            Tab.Title = Item.Track.Title;
            Tab.description = Item.Track.Description;
            Tabs.push(Tab);
        });
        $scope.Tabs = _.uniq(Tabs, function(Tab) {
            return Tab.Title;
        });
        $scope.ActiveTab = _.first($scope.Tabs).Title;

        //Load the first track details when the page loads
        $scope.setTrack(_.first($scope.Tabs).Title);
    };
    $scope.load();
}]);


